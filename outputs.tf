### pb specifica ###
output "prunux_networks_ipv4" {
  description = "prunux management networks"

  value = [
    "192.168.1.32/32", # FIXME: my *public* home IP
    "172.16.32.0/24",  # FIXME: my *public* office IP
  ]
}

output "prunux_networks_ipv6" {
  description = "prunux management networks"

  value = [
    "2001:16:16::/48", # FIXME: my *public* home IP
  ]
}

output "mgmt_key_name" {
  value = "my-mgmt@aws"
}

output "mgmt_key" {
  value = "${file("${path.module}/ssh-key/id_rsa.aws_example_com.pub")}"
}
